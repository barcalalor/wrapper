import functools

from tools.log import create_rotating_file_by_space


def ProcessLogger(logger):
    """
    A decorator that wraps the passed in function and logs
    exceptions should one occur

    @param logger: The logging object
    """

    def decorator(func):

        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except:
                err = "There was an exception in  "
                err += func.__name__
                logger.exception(err)
            raise

        return wrapper

    return decorator


def FileOutput(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        print(result)
        return result

    return wrapper
