import logging
from logging.config import fileConfig
from logging.handlers import TimedRotatingFileHandler, RotatingFileHandler


def create_time_logger(config, path=None, period=None, period_value=None):
    """
    Creates a logging object and returns it
    """
    logger = logging.getLogger("Rotating Time Log")
    logger.setLevel(logging.INFO)
    if not path:
        path = config['log_path']
    if not period:
        period = config['log_period']
    if not period_value:
        period_value = config['log_interval_period']

    handler = TimedRotatingFileHandler(path,
                                       when=period,
                                       interval=period_value,
                                       backupCount=5)
    logger.addHandler(handler)

    return logger


def create_rotating_file_by_space():
    logger = logging.getLogger('Max Storage Log')
    handler = RotatingFileHandler('logs/output.log', maxBytes=2000, backupCount=10)
    logger.addHandler(handler)
    return logger