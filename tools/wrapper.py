import json
from datetime import datetime
from subprocess import Popen

from tools.log import create_rotating_file_by_space


def get_default_params(config_file=None, test=False):
    """
    Method to instance the default configuration var
    :param config_file: path of the config file
    :param test: True if test
    :return:
    """
    if config_file:
        with open(config_file) as json_file:
            default_params = json.load(json_file)
    else:
        with open('./settings/config.json') as json_file:
            default_params = json.load(json_file)

    if test:
        default_params["output_path"] = "../logs/output_{}.log"
        default_params["log_path"] = "../logs/error_{}.log"

    if default_params:
        if not 'timeout' in default_params:
            default_params['timeout'] = None
    return default_params



def open_process(params, timeout=None):
    """
    Method to open instance process
    :param params:  config vars
    :param timeout: time of timeout
    :return: process
    """
    output_file = open(params["output_path"].format(datetime.today().strftime('%Y-%m-%d')), "a")
    error_file = open(params["log_path"].format(datetime.today().strftime('%Y-%m-%d')), "a")
    try:
        proc = Popen(params['default_process'].split(" "), stdout=output_file, stderr=error_file)
        proc.communicate(timeout=timeout)
        output_file.flush()
        error_file.flush()
        return proc
    except Exception as err:
        proc.kill()
        return proc


def wrapper_process(params, parallels=None, error_logger=None):
    """
    Method to reup the subprocess in case of exceptions and manage parallel executions
    :param params:
    :param parallels:
    :param error_logger:
    :return:
    """
    proc = None
    try:
        if parallels:
            for n in range(parallels):
                proc = open_process(params, timeout=params['timeout'])
        else:
            proc = open_process(params, timeout=params['timeout'])
    except Exception as err:
        if parallels:
            for n in range(parallels):
                proc = open_process(params, timeout=params['timeout'])
        else:
            proc = open_process(params, timeout=params['timeout'])
    finally:
        return proc
