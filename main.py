import sys

from tools.wrapper import get_default_params, wrapper_process

if __name__ == '__main__':
    #Get params from command line
    params = sys.argv
    timeout, config_file, parallels = None, None, None
    #check if params contains N params
    if len(params) > 2:
                config_file = params[1]
                parallels = int(params[2])
    if not config_file:
        #Instance configuration vars
        config = get_default_params()
    else:
        config = get_default_params(config_file)
    #Run wrapper
    wrapper_process(config, parallels=parallels)