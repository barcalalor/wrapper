from subprocess import Popen, PIPE
from time import sleep
from unittest import TestCase

from tools.wrapper import get_default_params, wrapper_process


class TestDoggy(TestCase):

    def test_run_process(self):
        config = get_default_params("../settings/config.json",test=True)
        proc = wrapper_process(config)
        assert proc.poll() == 1

    def test_run_parallel_process(self):
        config = get_default_params("../settings/config.json",test=True)
        proc = wrapper_process(config, parallels=2)
        assert proc.poll() == 1