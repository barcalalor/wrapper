# Test

## Objetivos a completar
El candidato habrá de diseñar y desarrollar la aplicación descrita
anteriormente.

Además, habrá de añadir dos funcionalidades a elegir de entre la
siguiente lista:

- [X] Testing de la aplicación. 

- [ ] Comprimir los ficheros cuando se rotan.

- [X] Leer los datos sobre la aplicación a correr (doggy) y otros
  parámetros de un fichero de configuración.

- [ ] Generar un fichero con estadísticas sobre el trabajo realizado hasta
  el momento al recibir una señal (p.ej. USR1).

- [X] Ser capaces de ejecutar N instancias de la aplicación doggy en
  paralelo.

### Explicacion

Para realizar la prueba decidi utilizar librerias nativas como subprocess, y proponerlo como un scrip ejecutable por consola.
El flujo de la aplicación consiste en:

- #### Carga de datos de configuracion iniciales:
    Aqui añadimos toda la información que se gestiona en el archivo de configuracion (config.json)
    
- #### Seleccionar si la ejecución sera unica o en paralelo y el numero de ejecuciones:
    Para ello se pueden añadir se ha de añadir por parametros tanto el path del archivo de configuracion como el numero 
    de ejecuciones.

        python main.py "../path/config.json" "2"

- #### Redireccionamos los buffers de entrada y salida hacia 2 archivos:
    En esta parte quise hacerlo con 2 clases de Logs pero este método no permitia redireccionar directamente desde el Popen
     podeis encontrar los ejemplos en el archivo log de la carpeta tools
 
 
 - #### En caso de darse alguna excepción se relanzan la/s ejecucion/es 
 
 
 
 A mayores he creado un par de decoradores para ir capturando los resultados del proceso sin llenar el programa de prints 
 los podeis encontrar en el archivo decorators
   